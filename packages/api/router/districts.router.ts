import express, { Request, Response } from "express";
import { ApiResponse } from "../models";
import {DB, District, Districts} from '@rohmer/db';
import {error, info} from '@rohmer/util';

export function districtsRouter() {
  const db = DB.getInstance()
  const r = express.Router();
  r.get("/", async (req: Request, res: Response<ApiResponse<Districts>>) => {
    await db.init()
    const repository = db.getRepository(District)
    const start = req.query.start ? +new Date(req.query.start as string) : null;
    const end = req.query.end ? +new Date(req.query.end as string) : null;
    let qb = repository.createQueryBuilder('d')
      .select()
    ;

    if (typeof req.query.sort === 'string') {
      const sort = req.query.sort.toLowerCase() === 'desc' ? 'DESC' : 'ASC'
      qb = qb
        .addOrderBy('d.created', sort)
        .addOrderBy('d.name', sort)
      ;
    }
    if (start) {
      qb = qb.andWhere('datetime(d.created) >= datetime(:value, "unixepoch")', {value: start / 1000});
    }
    if (end) {
      qb = qb.andWhere('datetime(d.created) <= datetime(:value, "unixepoch")', {value: end / 1000});
    }

    let data = [];
    try {
      data = await qb.getMany();
    } catch (e) {
      error(e);
      throw new Error("some unexpected error");
    }

    res.send({
      data,
    });
  });
  return r;
}
