import {RequestHandler} from 'express';
import { basic }from "http-auth";

export const authMiddleware = (htpasswdFile: string|undefined): RequestHandler => {
  if (htpasswdFile) {
    const basicAuth = basic({
      file: htpasswdFile,
    });
    return (req, res, next) => {
      basicAuth.check(((req1, res1) => {
        next();
      }))(req, res);
    };
  }

  return (req, res, next) => next();
}
