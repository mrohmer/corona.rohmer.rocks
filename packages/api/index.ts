export * from './models';
import express, { NextFunction, Request, Response } from "express";
import { error, info } from "@rohmer/util";

import cors from "cors";
import {districtsRouter} from './router/districts.router';
import {authMiddleware} from './auth-middleware';


const app = express();
const basePath = ".local";
const requestLogs = basePath + "/logs";
const errorLog = basePath + "/error.log";
const artifacts = basePath + "/artifacts";

app.use(cors());

app.use((req: Request, res: Response, next: NextFunction) => {
  info(req.method, req.originalUrl);
  next();
});

app.use(authMiddleware(process.env.HTPASSWD_FILE));

app.use("/districts", districtsRouter());
app.use((err: any, req: Request, res: Response, next: NextFunction) => {
  error(err.stack);
});
const port = process.env.PORT ?? 4201;
app.listen(port, () => {
  info(`Starting server on http://localhost:${port}`);
  info("Access Logs   =>", requestLogs);
  info("Error Logs    =>", errorLog);
  info("Artifacts =>", artifacts);
});
