
import * as path from 'path';

import {District} from './models/district';
import {Connection, createConnection, Repository} from 'typeorm';
import {EntityTarget} from 'typeorm/common/EntityTarget';

export class DB {
  static instance: DB;
  private connection: Connection;

  static getInstance(): DB {
    if (!DB.instance) {
      DB.instance = new DB();
    }
    return DB.instance;
  }

  init(): Promise<DB> {
    if (this.connection) {
      return Promise.resolve(this);
    }
    return createConnection({
      type: "sqlite",
      database: path.resolve(__dirname, "../../../db.sqlite"),
      entities: [
        District
      ],
      synchronize: true,
      logging: false,
    })
      .then(connection => this.connection = connection)
      .then(() => this);
  }

  getRepository<T>(target: EntityTarget<T>): Repository<T> {
    if (!this.connection) {
      throw new Error('DB not initialized');
    }
    return this.connection.getRepository(target);
  }

}
