import {Column, CreateDateColumn, Entity, PrimaryGeneratedColumn} from 'typeorm';

export type Districts = District[];

@Entity()
export class District {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
  @CreateDateColumn()
  created: Date;
  @Column()
  casesAmount: number;
  @Column()
  casesShiftSinceYesterday: number;
  @Column()
  casesPer100kResidents: number;
  @Column()
  casesLast7Days: number;
  @Column()
  deathsAmount: number;
  @Column()
  deathsShiftSinceYesterday: number;
  @Column()
  incidence: number;

  constructor(data?: {name?: string, cases?: {amount?: number, shiftSinceYesterday?: number, per100kResidents?: number, last7Days?: number}, deaths?: {amount?: number, shiftSinceYesterday?: number}, incidence?: number}) {
    this.name = data?.name ?? '';
    this.casesAmount = data?.cases?.amount ?? 0;
    this.casesShiftSinceYesterday = data?.cases?.shiftSinceYesterday ?? 0;
    this.casesPer100kResidents = data?.cases?.per100kResidents ?? 0;
    this.casesLast7Days = data?.cases?.last7Days ?? 0;
    this.deathsAmount = data?.deaths?.amount ?? 0;
    this.deathsShiftSinceYesterday = data?.deaths?.shiftSinceYesterday ?? 0;
    this.incidence = data?.incidence ?? 0;
  }
}
