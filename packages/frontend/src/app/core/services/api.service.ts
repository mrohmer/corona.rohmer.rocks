import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Districts} from '@rohmer/db';
import {ApiResponse} from '@rohmer/api';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {
  }

  getDistricts(start: Date): Observable<Districts> {
    const params: Record<string, string> = {};
    if (start) {
      params.start = start.toISOString();
    }
    return this.http.get<ApiResponse<Districts>>(
      environment.api,
      {
        params,
      }
    )
      .pipe(map(({data}) => data))
  }
}
