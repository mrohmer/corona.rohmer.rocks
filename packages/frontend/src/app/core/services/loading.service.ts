import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {delay, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  private loading$ = new BehaviorSubject(new Set<string>());
  isLoading$ = this.loading$
    .pipe(
      map(set => set.size !== 0),
      delay(1),
    );

  startLoading(key: string): void {
    this.loading$.next(
      new Set<string>(this.loading$.value).add(key)
    );
  }
  endLoading(key: string): void {
    const set = new Set<string>(this.loading$.value);
    set.delete(key)
    this.loading$.next(set);
  }
}
