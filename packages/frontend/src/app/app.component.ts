import { Component } from '@angular/core';
import {LoadingService} from './core/services/loading.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontend';

  sidebarOpen = false;

  constructor(public loadingService: LoadingService) {
  }

  onToolbarRightClick() {
    this.sidebarOpen = !this.sidebarOpen;
  }

}
