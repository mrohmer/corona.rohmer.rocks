import {
  AfterViewInit, ApplicationRef,
  ComponentFactoryResolver,
  Directive,
  ElementRef,
  Host,
  Injector,
  Input,
  OnDestroy
} from '@angular/core';
import {PortalService} from '../services/portal.service';
import {CdkPortal, DomPortalOutlet, PortalOutlet} from '@angular/cdk/portal';
import {ReplaySubject, Subject} from 'rxjs';
import {distinctUntilChanged, switchMap, takeUntil, tap} from 'rxjs/operators';

@Directive({
  selector: '[appPortalHost], app-portal-host'
})
export class PortalHostDirective implements AfterViewInit, OnDestroy {
  private destroyed$ = new Subject();
  private portalOutlet: PortalOutlet;
  private slot$ = new ReplaySubject<string>();

  @Input()
  set appPortalHost(value: string) {
    this.slot$.next(value);
  }
  @Input()
  set slot(value: string) {
    this.slot$.next(value);
  }

  attached = false;

  constructor(
    private portalService: PortalService,
    @Host() private hostRef: ElementRef,
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector,
    private appRef: ApplicationRef,
  ) {
  }

  ngAfterViewInit(): void {
    this.portalOutlet = new DomPortalOutlet(
      this.hostRef.nativeElement,
      this.componentFactoryResolver,
      this.appRef,
      this.injector
    );
    this.slot$
      .pipe(
        distinctUntilChanged(),
        tap(() => this.updateContentAttachment(null)),
        switchMap(slot => this.portalService.getSlotListener(slot)),
        takeUntil(this.destroyed$)
      )
      .subscribe(
        (portal) => this.updateContentAttachment(portal),
        () => {
        },
        () => this.updateContentAttachment(null)
      );
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
  private updateContentAttachment(portal: CdkPortal): void {
    if (this.portalOutlet.hasAttached()) {
      this.portalOutlet.detach();
    }
    if (portal) {
      this.portalOutlet.attach(portal);
    }
    this.attached = this.portalOutlet.hasAttached();
  }
}
