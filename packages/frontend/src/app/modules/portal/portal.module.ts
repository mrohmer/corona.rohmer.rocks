import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortalHostDirective } from './directives/portal-host.directive';
import { PortalModule as CdkPortalModule } from '@angular/cdk/portal';
import { PortalDirective } from './directives/portal.directive';


@NgModule({
  declarations: [PortalHostDirective, PortalDirective],
  imports: [
    CommonModule
  ],
  exports: [CdkPortalModule, PortalHostDirective, PortalDirective],
})
export class PortalModule { }
