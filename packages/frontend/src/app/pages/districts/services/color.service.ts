import { Injectable } from '@angular/core';
import * as randomColor from 'randomcolor';

@Injectable({
  providedIn: 'root'
})
export class ColorService {

  private colors: Record<string, string> = {};

  get(key: string): string {
    if (!(key in this.colors)) {
      this.colors[key] = this.createNewColor();
    }
    return this.colors[key];
  }
  private createNewColor(): string {
    const color = randomColor();
    return this.canUseColor(color) ? color : this.createNewColor();
  }
  private canUseColor(color: string): boolean {
    const colors = Object.values(this.colors);
    return !colors.includes(color);
  }
}
