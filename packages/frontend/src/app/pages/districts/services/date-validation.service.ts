import { Injectable } from '@angular/core';
import {getDateInDays} from '../utils/date';

@Injectable({
  providedIn: 'root'
})
export class DateValidationService {

  maxDate = new Date(getDateInDays(-1).setHours(0, 0, 0));
  minDate = new Date("2020-12-10 00:00:00");

  constructor() { }

  isValid(start: Date): boolean {
    if (!start) {
      return true;
    }

    return this.isNotAfterToday(start) && this.isNotBeforeValid(start);
  }
  isNotBeforeValid(date: Date): boolean {
    return this.isBefore(this.minDate, date);
  }
  isNotAfterToday(date: Date): boolean {
    return this.isBefore(date, this.maxDate);
  }
  isBefore(start: Date, end: Date): boolean {
    return +start <= +end;
  }
}
