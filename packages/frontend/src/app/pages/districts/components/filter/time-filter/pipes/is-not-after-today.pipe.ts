import { Pipe, PipeTransform } from '@angular/core';
import {DateValidationService} from '../../../../services/date-validation.service';

@Pipe({
  name: 'isNotAfterToday'
})
export class IsNotAfterTodayPipe implements PipeTransform {

  constructor(private dateValidator: DateValidationService) {
  }
  transform(date: Date): boolean {
    if (!date) {
      return true;
    }
    return this.dateValidator.isNotAfterToday(date);
  }

}
