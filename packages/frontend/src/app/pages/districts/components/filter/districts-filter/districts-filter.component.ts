import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Districts} from '@rohmer/db';
import {COMMA} from '@angular/cdk/keycodes';
import {BehaviorSubject, combineLatest} from 'rxjs';
import {map, startWith, take} from 'rxjs/operators';
import {FormControl} from '@angular/forms';
import {ColorService} from '../../../services/color.service';

@Component({
  selector: 'app-districts-filter',
  templateUrl: './districts-filter.component.html',
  styleUrls: ['./districts-filter.component.scss']
})
export class DistrictsFilterComponent {

  form = new FormControl();

  labels$ = new BehaviorSubject<string[]>([]);
  private originalSelected$ = new BehaviorSubject<string[]>([]);
  private selected$ = combineLatest([
    this.originalSelected$,
    this.labels$,
  ])
    .pipe(
      map(([selected, labels]) => selected.filter(selected => labels.includes(selected))),
    );
  unselectedLabels$ = combineLatest([
    this.selected$,
    this.labels$,
  ])
    .pipe(
      map(([selected, labels]) => labels.filter(label => !selected.includes(label))),
    );
  autocompleteValues$ = combineLatest([
    this.form.valueChanges
      .pipe(
        startWith(this.form.value),
        map(filter => filter ? filter.trim().toLowerCase() : filter),
      ),
    this.unselectedLabels$,
  ])
    .pipe(
      map(([filter, labels]) => {
        if (!filter) {
          return labels;
        }
        return labels
          .filter(label => label.toLowerCase().includes(filter));
      })
    );

  chips$ = this.selected$
    .pipe(
      map(selected => selected.sort()),
      map(selected => selected.map(
        label => ({
          label,
          color: this.colorService.get(label),
        })
      ))
    );

  separatorKeysCodes: number[] = [COMMA];

  @Input()
  set districts(value: Districts) {
    if (!value) {
      this.labels$.next([]);
      return;
    }
    this.labels$.next(Object
      .keys(value)
    );
  }

  @Input()
  set selected(selected: string[]) {
    this.originalSelected$.next(selected);
  }

  @Output()
  selectedChange = new EventEmitter<string[]>();

  constructor(private colorService: ColorService) {
  }

  addSelected(value: string): void {
    this.selected$
      .pipe(
        take(1)
      )
      .subscribe(selected => {
        if (selected.includes(value)) {
          return;
        }
        selected = [
          ...selected,
          value,
        ];
        this.selected = selected;
        this.selectedChange.emit(selected);
      });
  }

  removeSelected(value: string): void {
    this.selected$
      .pipe(
        take(1)
      )
      .subscribe(selected => {
        if (!selected.includes(value)) {
          return;
        }

        selected = selected.filter(selected => selected !== value);
        this.selected = selected;
        this.selectedChange.emit(selected);
      });
  }
}
