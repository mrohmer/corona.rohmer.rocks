import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Districts} from '@rohmer/db';
import {Resolution} from '../../models/types';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent {
  @Input()
  districts: Districts = [];
  @Input()
  selected: string[] = [];
  @Input()
  resolution: Resolution = 'day';
  @Input()
  start: Date;

  @Output()
  change = new EventEmitter<FilterValues>();

  onSubmit(valid: boolean): void {
    if (!valid) {
      return;
    }
    this.change
      .emit({
        selection: this.selected,
        resolution: this.resolution,
        start: this.start,
      })
  }
}
export interface FilterValues {
  resolution: Resolution;
  selection: string[];
  start: Date;
}
