import { Pipe, PipeTransform } from '@angular/core';
import {DateValidationService} from '../../../../services/date-validation.service';

@Pipe({
  name: 'isNotBeforeValid'
})
export class IsNotBeforeValidPipe implements PipeTransform {

  constructor(private dateValidator: DateValidationService) {
  }
  transform(date: Date): boolean {
    if (!date) {
      return true;
    }
    return this.dateValidator.isNotBeforeValid(date);
  }

}
