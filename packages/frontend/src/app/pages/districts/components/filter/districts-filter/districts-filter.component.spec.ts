import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DistrictsFilterComponent } from './districts-filter.component';

describe('DistrictsFilterComponent', () => {
  let component: DistrictsFilterComponent;
  let fixture: ComponentFixture<DistrictsFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DistrictsFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DistrictsFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
