import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Resolution} from '../../../models/types';

@Component({
  selector: 'app-resolution-filter',
  templateUrl: './resolution-filter.component.html',
  styleUrls: ['./resolution-filter.component.scss']
})
export class ResolutionFilterComponent {

  @Input()
  resolution: Resolution;
  @Output()
  resolutionChange = new EventEmitter<Resolution>();

}
