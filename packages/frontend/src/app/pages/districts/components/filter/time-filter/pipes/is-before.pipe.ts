import { Pipe, PipeTransform } from '@angular/core';
import {DateValidationService} from '../../../../services/date-validation.service';

@Pipe({
  name: 'isBefore'
})
export class IsBeforePipe implements PipeTransform {

  constructor(private dateValidator: DateValidationService) {
  }

  transform(start: Date, end: Date): boolean {
    if (!start || !end) {
      return true;
    }
    return this.dateValidator.isBefore(start, end);
  }

}
