import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DateValidationService} from '../../../services/date-validation.service';
import {ErrorStateMatcher} from '@angular/material/core';
import {getDateInDays} from '../../../utils/date';

@Component({
  selector: 'app-time-filter',
  templateUrl: './time-filter.component.html',
  styleUrls: ['./time-filter.component.scss']
})
export class TimeFilterComponent implements OnInit {

  @Input()
  start: Date;

  @Output()
  startChange = new EventEmitter<Date>();

  isValid = true;

  startErrorMatcher = this.createErrorMatcher(() => this.start);



  constructor(public dateValidator: DateValidationService) {
  }

  ngOnInit(): void {
  }

  onStartChange($event: Date) {
    this.start = $event;
    this.isValid = this.dateValidator.isValid($event);
    if (this.isValid) {
      this.startChange.emit($event);
    }
  }

  private createErrorMatcher(valueGetter: () => Date): ErrorStateMatcher {
    return {
      isErrorState: (): boolean => {
        const value = valueGetter();
        if (!value) {
          return false;
        }

        return !(
          this.dateValidator.isNotBeforeValid(value)
          && this.dateValidator.isNotAfterToday(value)
        )
      },
    };
  }
}
