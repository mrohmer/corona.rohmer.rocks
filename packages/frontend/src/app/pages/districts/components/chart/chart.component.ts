import {Component, Input} from '@angular/core';
import {combineLatest, ReplaySubject, Subject} from 'rxjs';
import {filter, map, pluck, tap} from 'rxjs/operators';
import {District, Districts} from '@rohmer/db';
import {Metric, Resolution} from '../../models/types';
import {ColorService} from '../../services/color.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent {

  private districts$ = new ReplaySubject<Districts>();
  private selected$ = new ReplaySubject<string[]>();
  private resolution$ = new ReplaySubject<Resolution>();
  private metric$ = new ReplaySubject<Metric>();

  @Input()
  set districts(value: Districts) {
    this.districts$.next(value);
  }
  @Input()
  set selected(value: string[]) {
    this.selected$.next(value);
  }
  @Input()
  set resolution(value: Resolution) {
    this.resolution$.next(value);
  }
  @Input()
  set metric(value: Metric) {
    this.metric$.next(value);
  }


  chartData$ = combineLatest([
    this.districts$,
    this.selected$,
    this.resolution$,
    this.metric$,
  ])
    .pipe(
      filter(([districts]) => !!districts),
      map(
        ([districts, selected, resolution, metric]) => [[].concat(
          ...Object.entries(districts)
            .filter(([key]) => selected.includes(key))
            .map(([_, value]) => value)
        ), resolution, metric] as [District[], Resolution, Metric]
      ),
      map(([districts, resolution, metric]) => {
        const createdDateFactor = 1000 * 60 * 60 * (resolution === 'day' ? 24 : 1);
        let districtNames = Object.keys(districts.reduce(
          (prev, curr) => ({
            ...prev,
            [curr.name]: true,
          }),
          {},
        ))
        let days: Record<number, Record<string, District>> = {};
        districts.forEach(district => {
          const day = Math.floor(+new Date(district.created as any as string) / createdDateFactor);
          days = {
            ...days,
            [day]: {
              ...(days[day] ?? {}),
              [district.name]: district,
            }
          }
        });

        const keys = Object.keys(days).sort();
        let datasets: Record<string, District[]> = {};

        keys.forEach(key => {
          districtNames.forEach(name => {
            datasets = {
              ...datasets,
              [name]: [
                ...(datasets[name] ?? []),
                name in days[key] && days[key][name] ? days[key][name] : null,
              ]
            }
          })
        });

        return {
          labels: keys.map(key => {
            const date = new Date(parseInt(key) * createdDateFactor);
            const dateStr = `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}`;
            const timeStr = `${date.getHours()}:${date.getMinutes().toFixed(0).padStart(2, '0')}`;
            return resolution === 'day' ? dateStr : [dateStr, timeStr].join(' ');
          }),
          datasets: Object.entries(datasets).map(([label, data]) => ({
            label,
            data: data.map(d => d && metric in d ? d[metric] : null),
            fill: false,
            borderColor: this.colorService.get(label),
          })),
        }
      }),
    );

  chartDataPoints$ = this.chartData$
    .pipe(
      pluck('datasets'),
      map(datasets => datasets.map(dataset => dataset.data.length)),
      map(lengths => lengths.sort()),
      map(lengths => lengths.length > 0 ? lengths[0] : 0),
    )

  constructor(private colorService: ColorService) {
  }
}
