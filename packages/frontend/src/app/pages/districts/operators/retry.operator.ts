import { MonoTypeOperatorFunction, timer } from 'rxjs';
import { retryWhen, switchMap } from 'rxjs/operators';

export const retryUntilSuccess = <T>(scalingDuration = 1000): MonoTypeOperatorFunction<T> => {
  let retryAttempt = 0;
  return retryWhen((errors) => errors.pipe(switchMap(() => timer(++retryAttempt * scalingDuration))));
};
