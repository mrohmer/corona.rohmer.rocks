import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'keys'
})
export class KeysPipe implements PipeTransform {

  transform(value: Record<string | number, any>): string[] {
    if (!value) {
      return [];
    }
    return Object.keys(value);
  }

}
