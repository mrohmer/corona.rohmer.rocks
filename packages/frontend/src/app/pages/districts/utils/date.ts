export const getDateInDays = (shift: number): Date =>
  new Date(new Date().setDate(new Date().getDate()+shift));
