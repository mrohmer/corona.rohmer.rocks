export const transformObjToIterable = <K extends string, V = any>(obj: Record<K, V>): {key: K, value: V}[] =>
  Object.entries(obj)
    .map(([key, value]) => ({key, value} as {key: K, value: V}));
;
