export type Resolution = 'hour' | 'day';

export enum Metric {
  INCIDENCE = 'incidence',
  CASE_AMOUNT = 'casesAmount',
  CASES_SHIFT_SINCE_YESTERDAY = 'casesShiftSinceYesterday',
  CASES_PER_100K_RESIDENTS = 'casesPer100kResidents',
  CASES_LAST_7_DAYS = 'casesLast7Days',
  DEATHS_AMOUNT = 'deathsAmount',
  DEATHS_SHIFT_SINCE_YESTERDAY = 'deathsShiftSinceYesterday',
}

export const METRICS_LABELS: Record<Metric, string> = {
  [Metric.INCIDENCE]: '7 Tage Inzidenzwert',
  [Metric.CASE_AMOUNT]: 'Fallzahl',
  [Metric.CASES_SHIFT_SINCE_YESTERDAY]: 'Fallzahlzuwachs seit Vortag',
  [Metric.CASES_PER_100K_RESIDENTS]: 'Fallzahl pro 100k Einwohner',
  [Metric.CASES_LAST_7_DAYS]: 'Fallzahl der vorherigen 7 Tage',
  [Metric.DEATHS_AMOUNT]: 'Tode',
  [Metric.DEATHS_SHIFT_SINCE_YESTERDAY]: 'Zuwachs an Toden seit Vortag',
};
