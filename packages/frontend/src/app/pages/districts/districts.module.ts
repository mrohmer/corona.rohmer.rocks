import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DistrictsRoutingModule } from './districts-routing.module';
import { DistrictsComponent } from './districts.component';
import { IncludesPipe } from './pipes/includes.pipe';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {ChartModule} from 'primeng/chart';
import { KeysPipe } from './pipes/keys.pipe';
import {MatRadioModule} from '@angular/material/radio';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { ChartComponent } from './components/chart/chart.component';
import { ResolutionFilterComponent } from './components/filter/resolution-filter/resolution-filter.component';
import { DistrictsFilterComponent } from './components/filter/districts-filter/districts-filter.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatChipsModule} from '@angular/material/chips';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {PortalModule} from '../../modules/portal/portal.module';
import { TimeFilterComponent } from './components/filter/time-filter/time-filter.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MAT_DATE_LOCALE, MatNativeDateModule} from '@angular/material/core';
import { FilterComponent } from './components/filter/filter.component';
import { IsNotAfterTodayPipe } from './components/filter/time-filter/pipes/is-not-after-today.pipe';
import { IsBeforePipe } from './components/filter/time-filter/pipes/is-before.pipe';
import { IsNotBeforeValidPipe } from './components/filter/time-filter/pipes/is-not-before-valid.pipe';


@NgModule({
  declarations: [DistrictsComponent, IncludesPipe, KeysPipe, ChartComponent, ResolutionFilterComponent, DistrictsFilterComponent, TimeFilterComponent, FilterComponent, IsNotAfterTodayPipe, IsBeforePipe, IsNotBeforeValidPipe],
  imports: [
    CommonModule,
    DistrictsRoutingModule,
    MatCheckboxModule,
    MatRadioModule,
    MatButtonModule,
    MatIconModule,
    MatExpansionModule,
    MatChipsModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatButtonToggleModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    ChartModule,
    FormsModule,
    ReactiveFormsModule,
    PortalModule,
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'de-DE'},
  ],
})
export class DistrictsModule { }
