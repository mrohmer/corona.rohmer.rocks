import {Component, OnDestroy, OnInit} from '@angular/core';
import {ApiService} from '../../core/services/api.service';
import {finalize, map, pluck, shareReplay, switchMap, take, takeUntil, tap} from 'rxjs/operators';
import {District} from '@rohmer/db';
import {combineLatest, Observable, OperatorFunction, Subject, timer} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {Metric, METRICS_LABELS, Resolution} from './models/types';
import {transformObjToIterable} from './utils/obj-to-iterable';
import {LoadingService} from '../../core/services/loading.service';
import {retryUntilSuccess} from './operators/retry.operator';
import {getDateInDays} from './utils/date';
import {FilterValues} from './components/filter/filter.component';

const dafaultSelection = [
  'Erlangen Stadt',
  'Fürth',
  'Fürth Stadt',
  'Nürnberg Stadt'
].join(',');

@Component({
  selector: 'app-districts',
  templateUrl: './districts.component.html',
  styleUrls: ['./districts.component.scss']
})
export class DistrictsComponent implements OnInit, OnDestroy {
  private destroyed$ = new Subject<void>();
  load$ = new Subject<void>();

  loading = true;

  readonly METRICS = transformObjToIterable(METRICS_LABELS);

  resolution$: Observable<Resolution> = this.activatedRoute
    .queryParams
    .pipe(
      pluck('resolution'),
      map(resolution => resolution ?? 'day'),
    )

  selected$ = this.activatedRoute
    .queryParams
    .pipe(
      pluck('selection'),
      map(selection => selection ?? dafaultSelection),
      map(selection => selection
        .split(',')
        .map(item => item
          .replace(/;/g, ',')
          .replace(/_/g, ' ')
        )),
    );
  start$ = this.activatedRoute
    .queryParams
    .pipe(
      pluck('start'),
      prepareDate(new Date(getDateInDays(-7).setHours(1, 0, 0))),
    );

  districts: Record<string, District[]> = {};

  constructor(
    private api: ApiService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private loadingService: LoadingService,
  ) {
  }

  ngOnInit(): void {
    combineLatest([
      this.start$,
      this.load$,
    ])
      .pipe(
        tap(() => {
          this.loading = true;
          this.loadingService.startLoading('/districts');
        }),
        switchMap(([start]) => this.api
          .getDistricts(start)
          .pipe(
            retryUntilSuccess(),
          )
        ),
        map(districts => districts.reduce(
          (prev, curr) => ({
            ...prev,
            [curr.name]: [
              ...(prev[curr.name] ?? []),
              curr,
            ]
          }),
          {} as Record<string, District[]>,
        )),
        tap(() => {
          this.loading = false;
          this.loadingService.endLoading('/districts');
        }),
        takeUntil(this.destroyed$),
      )
      .subscribe(districts => this.districts = districts)
    ;
    this.load$.next();
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  navigate(override: FilterValues): void {
    const queryParams = this.buildQueryParams(override);
    this.router.navigate(['.'], {queryParams});
  }

  private buildQueryParams(override: FilterValues): Record<string, string> {
      const params: Record<string, string> = {
        resolution: override.resolution,
        selection: (override.selection ?? [])
          .map(
            item => item
              .replace(/,/g, ';')
              .replace(/[ ]{2,}/g, ' ')
              .replace(/ /g, '_')
          )
          .join(','),
      };
      if (override.start) {
        params.start = String(+override.start / 1000);
      }

      return params;
  }
}
const prepareDate = (defaultValue?: Date): OperatorFunction<string, Date> => {
  return source => source
    .pipe(
      map(date => date ? parseInt(date) : null),
      map(date => date && !isNaN(date) ? date: null),
      map(date => date ? new Date(date * 1000) : defaultValue),
    );
};
