export class Stopwatch {
  protected starttime: Date;
  protected endtime: Date;

  static start(): Stopwatch {
    return new Stopwatch().start();
  }

  start(): this {
    this.starttime = new Date();
    return this;
  }
  end(): number {
    if (!this.starttime) {
      throw new Error('Stopwatch has not being started.');
    }
    this.endtime = new Date();
    return this.getDuration();
  }
  getDuration(): number {
    if (!this.starttime) {
      throw new Error('Stopwatch has not being started.');
    }
    return +(this.endtime ?? new Date()) - +this.starttime;
  }
}
