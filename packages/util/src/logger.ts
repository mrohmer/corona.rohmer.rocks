import chalk, { Chalk } from 'chalk';
import moment from "moment";

let logPath = "log";
let logfile = moment().format("YYYY-MM-DD-HH-mm-ss") + "-error.log";
let debugMode = false;

export function setDebugMode(enabled = true) {
  debugMode = enabled;
  info(enabled ? "Enabling" : "Disabling", "Debug Mode");
}

export function isDebugModeEnabled() {
  return debugMode;
}

let oddOrEven = false;

function logInternal(chalkFn: Chalk, strings: any[], prefix?: string) {
  const p = prefix ? chalk.inverse(" " + prefix.padEnd(9)) : "";
  const strOut = chalk.bold(p, getCallerFile()) + " " + strings.join(" ");
  if ((oddOrEven = !oddOrEven)) {
    console.info(chalkFn.bgRgb(20, 20, 20)(strOut.padEnd(process.stdout.columns)));
  } else {
    console.info(chalkFn.bgRgb(25, 25, 25)(strOut.padEnd(process.stdout.columns)));
  }
}

export function log(chalkFn: Chalk, strings: any[], prefix?: string) {
  logInternal(chalkFn, strings, prefix);
}

export function success(...strings: any[]) {
  logInternal(chalk.rgb(0, 200, 0), strings, "Success");
}

export function debug(...strings: any[]) {
  if (debugMode) logInternal(chalk.hex("FF00FF"), strings, "Debug");
}

export function info(...strings: any[]) {
  logInternal(chalk.blue, strings, "Info");
}

export function warn(...strings: any[]) {
  logInternal(chalk.yellowBright, strings, "Warning");
}

export function error(...strings: any[]) {
  logInternal(chalk.rgb(235, 20, 20), strings, "Error");
}

let basePath = process.cwd();
let pattern = new RegExp(basePath.replace(/\//g, "\\/") + "\\/(.*?:\\d+:\\d+)");
/**
 * @param traverse how many calls to move up the stack tree
 * first is this function
 * next is usually the error handler above
 */
function getCallerFile(traverse: number = 3): string | null {
  const err = new Error();
  // Regex magic to parse out the first file from the callstack
  return (
    err.stack?.split(/\r?\n/)[
      // [traverse + 1].match(/ \(?(.*?:\d+:\d+)\)?$/)?.[1]
    traverse + 1
      ].match(pattern)?.[1] || null
  );
}
