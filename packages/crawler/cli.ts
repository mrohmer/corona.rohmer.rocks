import {info, setDebugMode} from '@rohmer/util';
import {crawl} from './src/crawler';
import {DB, District, Districts} from '@rohmer/db';

setDebugMode(true);

const db = DB.getInstance();
async function exec(): Promise<Districts> {
  await db.init();
  const repository = db.getRepository(District);
  const data = await crawl();

  await repository.save(data);

  return data
}

exec().then(data => info('inserted data for', data.length, 'districts'));

