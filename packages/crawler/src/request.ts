import axios, { AxiosRequestConfig, AxiosResponse } from "axios";

export async function get<T>(options: AxiosRequestConfig): Promise<T> {
  if (options.url) {
    options.url = encodeURI(options.url);
  }

  const originalConfig = {
    ...options,
  };
  const {data} = await axios.request<T>(originalConfig);

  return data;
}
