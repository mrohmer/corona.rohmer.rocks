import {Districts, District} from '@rohmer/db';
import cheerio from 'cheerio';
import numeral from 'numeral';
import {debug} from '@rohmer/util';

numeral.register('locale', 'de', {
  delimiters: {
    thousands: ' ',
    decimal: ','
  },
  abbreviations: {
    thousand: 'k',
    million: 'm',
    billion: 'b',
    trillion: 't'
  },
  ordinal : function (number) {
    return 'asdf?!';
  },
  currency: {
    symbol: '€'
  }
});
numeral.locale('de');


function parseGermanFloat(value: string): number {
  return numeral(value).value();
}
function parseShift(value: string): number {
  return parseGermanFloat(
    value.replace(/[ ()]*/g, '')
  )
}
export function parse(html: string): Districts {
  const $ = cheerio.load(html);
  const districts: Districts = [];
  $('#tableLandkreise tr').each((_: any, element: any) => {
    const name = $(element).find('td:nth-child(1)').text();
    if (!name || name === 'Gesamtergebnis') {
      return;
    }
    const district: District = new District({
      name: `${name}`,
      cases: {
        amount: parseGermanFloat($(element).find('td:nth-child(2)').text()),
        shiftSinceYesterday: parseShift($(element).find('td:nth-child(3)').text()),
        per100kResidents: parseGermanFloat($(element).find('td:nth-child(4)').text()),
        last7Days: parseGermanFloat($(element).find('td:nth-child(5)').text()),
      },
      incidence: parseGermanFloat($(element).find('td:nth-child(6)').text()),
      deaths: {
        amount: parseGermanFloat($(element).find('td:nth-child(7)').text()),
        shiftSinceYesterday: parseShift($(element).find('td:nth-child(8)').text()),
      },
    })

    debug('district', name, JSON.stringify(district));
    districts.push(district);
  });
  return districts;
}
