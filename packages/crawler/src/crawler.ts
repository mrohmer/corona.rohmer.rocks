import {debug, info, Stopwatch} from '@rohmer/util';
import {get} from './request';
import {Districts} from '@rohmer/db';
import {parse} from './parse';

async function fetchHtml(): Promise<string> {
  const url = 'https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/';
  debug('start fetching', url);
  const stopwatch = Stopwatch.start();
  const html = await get<string>({url});
  const duration = stopwatch.end();
  debug('finish fetching after', duration, 'ms');
  return html;
}
async function parseHtml(html: string): Promise<Districts> {
  debug('start parsing');
  const stopwatch = Stopwatch.start();
  const parsed = parse(html);
  const duration = stopwatch.end();
  debug('finish parsing after', duration, 'ms');
  return parsed;
}
export async function crawl(): Promise<Districts> {
  const html = await fetchHtml();
  return parseHtml(html);
}
