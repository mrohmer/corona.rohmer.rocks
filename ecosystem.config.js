module.exports = {
  apps: [
    {
      name: "api-corona.rohmer.rocks",
      script: "packages/api/index.js",
      watch: false,
      env: {
        PORT: 8001,
        HTPASSWD_FILE: "/etc/apache2/corona.rohmer.rocks.htpasswd"
      },
    },
    {
      name: "crawler-corona.rohmer.rocks",
      script: "packages/crawler/cli.js",
      cron_restart: "0 * * * *",
      watch: false,
      autorestart: false,
    },
  ],
};
